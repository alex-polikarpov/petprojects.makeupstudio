# Методы Api для работы с заказами

## /Orders/CreateBid

Post метод. Параметры:
* CompanyId - Id фирмы, строка;
* ClientPhone - номер телефона клиента, строка;
* ClientName - имя клиента, строка;
* ClientComment - комментарий клиента, строка.

**Пример запроса**:

```
{
  "CompanyId": "67249fb6-1399-4faa-996d-b61150c4cd7e",
  "ClientPhone": "89090033456",
  "ClientName" : "Антон",
  "ClientComment" : ""
}
```

**Пример ответа**:
```
{
  "ErrorMessage": null,
  "ErrorCode": 0
}

```