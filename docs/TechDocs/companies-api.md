# Методы Api для получения информации о фирмах

## /Companies/GetCompaniesForMap

Метод Get, не принимает параметров.

Возвращает список активных компаний, для карты. Возвращает только те компании, у которых есть услуги с установленной ценой


**Пример ответа**: 
```
{  
   "Companies":[  
      {  
         "Id":"67249fb6-1399-4faa-996d-b61150c4cd7e",
         "Name":"Тестовая фирма",
         "Description":"Это тестовая фирма, созданная автоматически",
         "City":"Казань",
         "Street":"Дубравная",
         "HouseNumber":"11",
         "Latitude":55.741404,
         "Longitude":49.2165626
      },
      {  
         "Id":"6a4a9b5d-01dd-48ba-ae81-f823015593a6",
         "Name":"Вторая тестовая фирма",
         "Description":null,
         "City":"Казань",
         "Street":"Толстого",
         "HouseNumber":"9",
         "Latitude":55.7916864,
         "Longitude":49.1376807
      }
   ],
   "ErrorMessage":null,
   "ErrorCode":0
}

```

## /Companies/GetCompaniesPreview

HttpGet, параметры передавать в URL.

Возвращает информацию по компаниям для превью в соответствии с фильтрами.

**Пример запроса**:

http://localhost:51238//Companies/GetCompaniesPreview?CategoriesId=17&CategoriesId=41&CategoriesId=38&minprice=10&maxprice=10000

* CategoriesId - массив целочисленных значений. Передавать идентификаторы категорий.
* MinPrice - минимальная цена на услугу. Целое число.
* MaxPrice - максимальная цена на услугу. Целое число.

**Пример ответа**:

```
{  
   "Companies":[  
      {  
         "Id":"67249fb6-1399-4faa-996d-b61150c4cd7e",
         "Name":"Тестовая фирма",
         "MinPrice":100.00,
         "Photos":[  
            "http://localhost:50716/Files/Photos/2/5/2516d8d48a95429fbc3e79ad76125c41.png?width=100&height=100&mode=carve",
            "http://localhost:50716/Files/Photos/9/d/9d2918e3f79f44d8a4fbb3b642b31e05.png?width=100&height=100&mode=carve",
            "http://localhost:50716/test?width=100&height=100&mode=carve"
         ],
         "PhotosCount":3,
         "Avatar":"http://localhost:50716/Content/Media/no_image.png?width=100&height=100&mode=carve"
      },
      {  
         "Id":"6a4a9b5d-01dd-48ba-ae81-f823015593a6",
         "Name":"Вторая тестовая фирма",
         "MinPrice":1000.00,
         "Photos":[  
            "http://localhost:50716/Files/Photos/5/e/5e86b27f4bb6416f81c8a16e8117ae26.jpg?width=100&height=100&mode=carve",
            "http://localhost:50716/Files/Photos/f/9/f90cd57a208b4019bf86df301e1212cd.jpg?width=100&height=100&mode=carve",
            "http://localhost:50716/Files/Photos/1/5/15020adf96774c5789a0241eb24de33d.jpg?width=100&height=100&mode=carve"
         ],
         "PhotosCount":3,
         "Avatar":"http://localhost:50716/Files/Photos/5/2/529c6569cc9340a683972657c563375e.jpg?width=100&height=100&mode=carve"
      }
   ],
   "ErrorMessage":null,
   "ErrorCode":0
}
```

## /Companies/GetCompanyInfo

Get метод. Параметр передавать в URL.

Возвращает информацию об одной конкретной фирме - ее фотографии, все услуги и цены.
Использовать для странички просмотра конкретной фирмы / мастера.

**Пример запроса**:

http://localhost:51238/Companies/GetCompanyInfo?companyId=67249fb6-1399-4faa-996d-b61150c4cd7e

companyId - это Id фирмы / мастера.


**Пример ответа:**
```
{  
   "CompanyInfo":{  
      "Id":"67249fb6-1399-4faa-996d-b61150c4cd7e",
      "Name":"Тестовая фирма",
      "Description":"Это тестовая фирма, созданная автоматически",
      "City":"Казань",
      "Street":"Дубравная",
      "HouseNumber":"11",
      "Latitude":55.741404,
      "Longitude":49.2165626
   },
   "Avatar":"http://localhost:50716/Content/Media/no_image.png?width=210",
   "Photos":[  
      "http://localhost:50716/Files/Photos/2/5/2516d8d48a95429fbc3e79ad76125c41.png?width=210",
      "http://localhost:50716/Files/Photos/9/d/9d2918e3f79f44d8a4fbb3b642b31e05.png?width=210",
      "http://localhost:50716/test?width=210",
      "http://localhost:50716/test23?width=210",
      "http://localhost:50716/test2345?width=210"
   ],
   "Categories":[  
      {  
         "CategoryId":17,
         "CategoryName":"Массаж",
         "HasWorks":false,
         "ChildCategories":[  
            {  
               "CategoryId":39,
               "CategoryName":"Антицеллюлитный массаж",
               "HasWorks":true,
               "ChildCategories":null,
               "Works":[  
                  {  
                     "Id":1,
                     "CategoryId":39,
                     "Name":"Тестовая услуга",
                     "Price":100.00
                  }
               ]
            },
            {  
               "CategoryId":41,
               "CategoryName":"Вакуумный массаж",
               "HasWorks":true,
               "ChildCategories":null,
               "Works":[  
                  {  
                     "Id":2,
                     "CategoryId":41,
                     "Name":"Тестовая услуга",
                     "Price":100.00
                  }
               ]
            },
            {  
               "CategoryId":37,
               "CategoryName":"Классический массаж",
               "HasWorks":true,
               "ChildCategories":null,
               "Works":[  
                  {  
                     "Id":3,
                     "CategoryId":37,
                     "Name":"Тестовая услуга",
                     "Price":100.00
                  }
               ]
            },
            {  
               "CategoryId":38,
               "CategoryName":"Лечебный массаж",
               "HasWorks":true,
               "ChildCategories":null,
               "Works":[  
                  {  
                     "Id":4,
                     "CategoryId":38,
                     "Name":"Тестовая услуга",
                     "Price":100.00
                  }
               ]
            },
            {  
               "CategoryId":40,
               "CategoryName":"Медовый массаж",
               "HasWorks":true,
               "ChildCategories":null,
               "Works":[  
                  {  
                     "Id":5,
                     "CategoryId":40,
                     "Name":"Тестовая услуга",
                     "Price":100.00
                  }
               ]
            }
         ],
         "Works":null
      }
   ],
   "ErrorMessage":null,
   "ErrorCode":0
}

```


