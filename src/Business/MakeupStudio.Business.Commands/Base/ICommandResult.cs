﻿namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Результат выполнения бизнес команды
    /// </summary>
    public interface ICommandResult
    {
        /// <summary>
        /// True, если команда выполнилась успешно, иначе false
        /// </summary>
        bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        string ErrorMessage { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        int ErrorCode { get; set; }
    }
}
