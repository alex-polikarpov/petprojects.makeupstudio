﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Business.Errors;
using MakeupStudio.Common.Utils.Cryptography;
using MakeupStudio.Common.Utils.Geolocation;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда регистрации пользователя в админке
    /// </summary>
    internal class RegistrationCommand : BaseCommand<RegistrationCommandArgs, EmptyCommandResult>
    {
        #region Конструктор, поля

        private readonly IGeolocationUtility _geolocationUtility;
        private readonly ICryptographyUtility _cryptographyUtility;
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _unitOfWork;
        private GeolocationInfo _geolocationInfo;

        public RegistrationCommand(
            ILoggerUtility loggerUtility,
            IGeolocationUtility geolocationUtility,
            ICryptographyUtility cryptographyUtility,
            IAccountService accountService,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _geolocationUtility = geolocationUtility;
            _cryptographyUtility = cryptographyUtility;
            _accountService = accountService;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region PerformCommand

        protected override async Task<EmptyCommandResult> InternalExecute(RegistrationCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            var company = CreateCompany(arguments);
            var account = CreateAccount(arguments);
            account.Company = company;
            _unitOfWork.GetRepository<Company>().Add(company);
            _unitOfWork.GetRepository<Account>().Add(account);
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        #endregion

        #region Validate

        protected override async Task<EmptyCommandResult> Validate(RegistrationCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            //Проверяем уникальность логина
            var exists = await _accountService.UserIsExistsAsync(arguments.Email);

            if (exists)
                return result.AddError(ErrorConstants.Accounts.EMAIL_ALREADY_USED, 10003);

            var phoneIsUsed = await _accountService.PhoneIsUsed(arguments.Phone);

            if (phoneIsUsed)
                return result.AddError(ErrorConstants.Accounts.PHONE_ALREADY_USED, 1000);

            //Пытаемся получить координаты адреса
            try
            {
                _geolocationInfo =
                    await _geolocationUtility.GetLocationByAddress(arguments.City, arguments.Street, arguments.House);

                if (_geolocationInfo == null)
                    return result.AddError(ErrorConstants.Accounts.GEOLOCATION_ERROR, 10002);
            }
            catch
            {
                return result.AddError(ErrorConstants.Accounts.GEOLOCATION_ERROR, 10002);
            }

            return result;
        }

        #endregion

        #region Вспомогательные методы

        public Company CreateCompany(RegistrationCommandArgs arguments)
        {
            return new Company()
            {
                Id = Guid.NewGuid(),
                Name = arguments.Name,
                Street = arguments.Street,
                City = arguments.City,
                CompanyState = CompanyState.Active,
                CompanyType = CompanyType.Individual,
                HouseNumber = arguments.House,
                Latitude = _geolocationInfo.Latitude,
                Longitude = _geolocationInfo.Longitude,                
            };
        }

        public Account CreateAccount(RegistrationCommandArgs arguments)
        {
            return new Account()
            {
                Email = arguments.Email,
                Phone = arguments.Phone,
                Name = arguments.Name,
                AccountState = AccountState.Active,
                PasswordHash = _cryptographyUtility.GetHash(arguments.Password)                
            };
        }

        #endregion
    }
}
