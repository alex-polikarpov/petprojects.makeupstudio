﻿using System.Threading.Tasks;
using MakeupStudio.Business.Errors;
using MakeupStudio.Common.Utils.Cryptography;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда авторизации пользователя
    /// </summary>
    internal class AuthorizeUserCommand : BaseCommand<AuthorizeUserCommandArgs, AuthorizeUserCommandResult>
    {
        private readonly ICryptographyUtility _cryptographyUtility;
        private readonly IAccountService _accountService;
        private Account _account;

        public AuthorizeUserCommand(
            ILoggerUtility loggerUtility,
            ICryptographyUtility cryptographyUtility,
            IAccountService accountService) : base(loggerUtility)
        {
            _cryptographyUtility = cryptographyUtility;
            _accountService = accountService;
        }

        protected override async Task<AuthorizeUserCommandResult> InternalExecute(AuthorizeUserCommandArgs arguments)
        {
            var result = new AuthorizeUserCommandResult() { IsSuccess = true };
            result.Account = _account;
            return result;
        }

        protected override async Task<AuthorizeUserCommandResult> Validate(AuthorizeUserCommandArgs arguments)
        {
            var result = new AuthorizeUserCommandResult() { IsSuccess = true };

            var hash = _cryptographyUtility.GetHash(arguments.Password);
            _account = await _accountService.GetAccountAsync(arguments.Login, hash);

            if (_account == null)
            {
                result.AddError(ErrorConstants.Accounts.INVALID_LOGIN_OR_PASS, 10001);
                return result;
            }

            if (_account.AccountState == AccountState.NotConfirmed)
            {
                return result.AddError(ErrorConstants.Accounts.ACCOUNT_NOT_VERIFIED, 10005);
            }

            if (_account.AccountState != AccountState.Active)
            {
                return result.AddError(ErrorConstants.Accounts.INVALID_ACCOUNT_STATE, 10006);
            }

            return result;
        }
    }
}
