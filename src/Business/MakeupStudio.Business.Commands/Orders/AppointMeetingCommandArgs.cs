﻿using System;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды назначения встречи клиенту
    /// </summary>
    public class AppointMeetingCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// Время, на которое назначен прием для клиента (ко скольки клиент должен подойти) UTC
        /// </summary>
        public DateTime AppointedTime { get; set; }

        /// <summary>
        /// Комментарий мастера к заказу
        /// </summary>
        public string MasterComment { get; set; }
    }
}
