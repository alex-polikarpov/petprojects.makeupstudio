﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Пометить заказ как выполненный
    /// </summary>
    internal class FinishOrderCommand : BaseCommand<FinishOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IOrderService _orderService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountService _accountService;
        private Order _order;

        public FinishOrderCommand(
            ILoggerUtility loggerUtility,
            IOrderService orderService,
            IUnitOfWork unitOfWork,
            IAccountService accountService) : base(loggerUtility)
        {
            _orderService = orderService;
            _unitOfWork = unitOfWork;
            _accountService = accountService;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(FinishOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            _order.OrderState = OrderState.Finished;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(FinishOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            //Проверяем что пользователь и фирма не заблокированы
            //
            var isActive = await _accountService.UserAndCompanyIsActive(
                arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if(!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);

            _order = await _orderService.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null || _order.OrderState != OrderState.Accepted)
                return result.AddError(Errors.ErrorConstants.Orders.ORDER_NOT_FOUND, 100);

            return result;
        }
    }
}
