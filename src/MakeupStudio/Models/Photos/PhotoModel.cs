﻿namespace MakeupStudio.Models.Photos
{
    /// <summary>
    /// Модель для отображения фотографий
    /// </summary>
    public class PhotoModel
    {
        public string Url { get; set; }

        public int Id { get; set; }
    }
}