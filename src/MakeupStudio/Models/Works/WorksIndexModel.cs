﻿using MakeupStudio.Core.Services;

namespace MakeupStudio.Models.Works
{
    /// <summary>
    /// Модель для отображения главной страницы услуг
    /// </summary>
    public class WorksIndexModel
    {
        /// <summary>
        /// Список услуг
        /// </summary>
        public WorkPreviewDto[] Works { get; set; }
    }
}