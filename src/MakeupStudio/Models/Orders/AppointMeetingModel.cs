﻿using System;

namespace MakeupStudio.Models.Orders
{
    /// <summary>
    /// Модель для страницы приема заявки
    /// </summary>
    public class AppointMeetingModel
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// Дата, когда оставили заявку (UTC)
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Комментарий клиента
        /// </summary>
        public string ClientComment { get; set; }
    }
}