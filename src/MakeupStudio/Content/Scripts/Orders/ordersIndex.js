﻿$(document).ready(function() {

    //=============================================================
    //=============================================================
    // Константы, переменные
    //=============================================================
    //=============================================================
    var notAcceptedOrders = [];
    var $helpText = $("#help-text");
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");
    var template = $('#table-template').html();
    var $tableContainer = $("#ordersTable");
    var $successContainer = $("#success-msg-container");
    var $successText = $("#success-msg");


    //=============================================================
    //=============================================================
    //Методы для работы с контролами
    //=============================================================
    //=============================================================

    //Рендерит таблицу
    //
    function renderTable() {
        var templateView = {
            orders:notAcceptedOrders,
            toLocateDateTime: _convertToLocalTime,
            hasOrders: notAcceptedOrders.length > 0
        };

        var rendered = Mustache.render(template, templateView);
        $tableContainer.html(rendered);
        setHelpText();
        $(".remove-order-btn").click(deleteBtn_OnClick);
    }

    //Устанавливает текст подсказку
    //
    function setHelpText() {
        var hasOrders = false;

        if (notAcceptedOrders && notAcceptedOrders.length > 0) 
            hasOrders = true;

        if (hasOrders) {
            $helpText.text('У Вас есть новые заявки. Созвонитесь с клиентами, назначьте время приема, после этого нажмите на зеленую кнопку "Принять".');
        } else {
            $helpText.text("Новых заявок от клиентов нет.");
        }       
    }

    //Обработчик события нажатия на кнопке удалить
    //
    function deleteBtn_OnClick() {
        _hideError();
        _hideSuccess();

        if (confirm("Вы действительно хотите удалить заявку?")) {
            var orderId = $(this).data("id");
            _deleteOrder(orderId);
        }
    }



    //=============================================================
    //=============================================================
    // AJAX методы и вспомогательные методы
    //=============================================================
    //=============================================================

    //Ajax метод для получения списока заявок
    //
    function _getNotAcceptedOrders() {

        var $deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: "/Orders/GetNotAcceptedOrders",
            success: function (data) {
                notAcceptedOrders = data;
                $deferred.resolve();
            },
            error: function (data) {
                $deferred.reject();
                _showError("Внутренняя ошибка сервера, перезагрузите страницу");
            }
        });

        return $deferred.promise();
    }

    //Ajax метод для удаления заявки
    //
    function _deleteOrder(orderId) {
        var $deferred = $.Deferred();

        $.ajax({
            type: "POST",
            url: "/Orders/DeleteOrder",
            data: {
                orderId : orderId
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {
                    _showError(data.ErrorMessage);
                } else {
                    notAcceptedOrders = jQuery.grep(notAcceptedOrders, function (value) {
                        return value.Id != orderId;
                    });

                    renderTable();
                    _showSuccess("Заявка успешно удалена");
                }
            },
            error: function () {
                $deferred.reject();
                _showError("Внутренняя ошибка сервера, перезагрузите страницу");
            }
        });

        return $deferred.promise();
    }

    //Конвертирует дату в локальное время и формирует строку с датой
    //
    function _convertToLocalTime() {
        var date = new Date(this.CreatedDate);
        var str = '';
        var val;

        str += (val = date.getDate()) < 10 ? "0" + val + "." : val + ".";
        str += (val = date.getMonth() + 1) < 10 ? "0" + val + "." : val + ".";
        str += date.getFullYear() + " ";
                
        str += (val = date.getHours()) < 10 ? "0" + val + ":" : val + ":";
        str += (val = date.getMinutes()) < 10 ? "0" + val: val;
        return str;
    }

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }

    //Отображает успешное сообщение
    //
    function _showSuccess(successMsg) {
        $successText.text(successMsg);
        $successContainer.show();
    }

    //Скрывает успешное сообщение
    //
    function _hideSuccess() {
        $successContainer.hide();
    }



    //=============================================================
    //=============================================================
    //INIT
    //=============================================================
    //=============================================================

    function init() {
        Mustache.parse(template);

        //Получаем данные с заявками и рендерим таблицу
        _getNotAcceptedOrders().done(function () {
            renderTable();            
        });               
    }

    init();
});