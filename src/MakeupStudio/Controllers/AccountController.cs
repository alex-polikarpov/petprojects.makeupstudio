﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MakeupStudio.Attributes;
using MakeupStudio.Autentication;
using MakeupStudio.Business.Commands;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Services;
using MakeupStudio.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace MakeupStudio.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet, DenyAuthorized]
        public ActionResult Login()
        {       
            return View(new LoginModel());
        }

        [HttpPost, DenyAuthorized, ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var arguments = CreateCommandArgs<AuthorizeUserCommandArgs>();
            arguments.Login = model.Email?.Trim();
            arguments.Password = model.Password?.Trim();

            var result = await ExecuteCommand<AuthorizeUserCommandArgs, AuthorizeUserCommandResult>(arguments);
            if (!result.IsSuccess)
            {
                SetErrorMessage(result.ErrorMessage);
                return View(model);
            }

            SaveAccountInCookies(result);
            return RedirectToAction("Index", "Orders");
        }

        [HttpGet, Authorize]
        public ActionResult LogOut()
        {
            var manager = HttpContext.GetOwinContext().Authentication;
            manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        [HttpGet, DenyAuthorized]
        public ActionResult Registration()
        {   
            return View(new RegistrationModel());
        }

        [HttpPost, DenyAuthorized, ValidateAntiForgeryToken]
        public async Task<ActionResult> Registration(RegistrationModel model)
        {
            if(!ModelState.IsValid)
                return View(model);

            var arguments = CreateCommandArgs<RegistrationCommandArgs>();
            arguments.Email = model.Email?.Trim();
            arguments.Phone = model.Phone?.Trim();
            arguments.Name = model.Name?.Trim();
            arguments.House = model.House?.Trim();
            arguments.Street = model.Street?.Trim();
            arguments.City = "Казань";
            arguments.Password = model.Password?.Trim();

            var result = await ExecuteCommand<RegistrationCommandArgs, EmptyCommandResult>(arguments);
            SetMessage(result, "Регистрация успешно завершена, теперь Вы можете зайти в личный кабинет");

            if (!result.IsSuccess)
                return View(model);            

            return View("Login", new LoginModel());
        }


        #region Вспомогательные методы

        /// <summary>
        /// Сохраняет в куки инфу о юзере
        /// </summary>
        private void SaveAccountInCookies(AuthorizeUserCommandResult account)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore());

            var identity = userManager.CreateIdentity(new ApplicationUser
            {
                Id = account.Account.Id.ToString(),
                UserName = account.Account.Name,               
            }, DefaultAuthenticationTypes.ApplicationCookie);

            identity.AddClaim(new Claim(ClaimTypes.Email, account.Account.Email.ToLower()));
            identity.AddClaim(new Claim(ClaimTypes.MobilePhone, account.Account.Phone.ToLower()));
            identity.AddClaim(new Claim(CustomClaimsConstants.COMPANY_ID, account.Account.CompanyId.ToString()));
            identity.AddClaim(new Claim(CustomClaimsConstants.ACCOUNT_STATE, account.Account.AccountState.ToString()));

            var manager = HttpContext.GetOwinContext().Authentication;
            var a = manager.GetAuthenticationTypes();


            manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            manager.SignIn(new AuthenticationProperties()
            {
                IsPersistent = true
            }, identity);
        }

        #endregion
    }
}