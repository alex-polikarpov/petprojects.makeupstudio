﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using MakeupStudio.Attributes;
using MakeupStudio.Common.Constants;

namespace MakeupStudio.Requests
{
    /// <summary>
    /// Запрос на добавление новой аватарки
    /// </summary>
    public class AddPhotoRequest
    {
        [Required(ErrorMessage = "Выберите картинку")]
        [ValidateFileExtension(".jpg", ".jpeg", ".png", ErrorMessage = ValidationErrors.INCORRECT_PHOTO_EXTENSION)]
        [ValidateFileSize(2048, ErrorMessage = ValidationErrors.PHOTO_INCORRECT_SIZE)]
        public HttpPostedFileBase Photo { get; set; }
    }
}