﻿using System.Web.Optimization;

namespace MakeupStudio
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterStyles(bundles);
            RegisterScripts(bundles);            
        }

        private static void RegisterStyles(BundleCollection bundles)
        {
            //Для выпадашек использовать https://silviomoreto.github.io/bootstrap-select/examples/
            //иначе обычный select у бутстрапа не отображается в мобилках
            bundles.Add(new StyleBundle("~/bundles/styles/bootstrap").Include(
                "~/Content/Libs/Bootstrap/css/bootstrap.min.css",
                "~/Content/Libs/Bootstrap/css/bootstrap-theme.min.css",
                "~/Content/Libs/Bootstrap/css/bootstrap-select.min.css",
                "~/Content/Styles/Common/errors.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/account/login").Include(
                "~/Content/Styles/Account/login.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/layout/admin").Include(
                "~/Content/Styles/AdminLayout/dashboard.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/photos/editavatar").Include(
                "~/Content/Libs/Bootstrap/css/bootstrap-fileinput.min.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/photos/addphoto").Include(
                "~/Content/Libs/Bootstrap/css/bootstrap-fileinput.min.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/orders/assignappointment").Include(
                "~/Content/Libs/Bootstrap/css/bootstrap-datetimepicker.min.css"));

            bundles.Add(new StyleBundle("~/bundles/styles/datepicker").Include(
                "~/Content/Libs/Bootstrap/css/bootstrap-datetimepicker.min.css"));
        }

        private static void RegisterScripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts/bootstrap").Include(
                "~/Content/Libs/JQuery/jquery-3.1.0.min.js",
                "~/Content/Libs/Bootstrap/js/bootstrap.min.js",
                "~/Content/Libs/Bootstrap/js/bootstrap-select.min.js"));

           bundles.Add(new ScriptBundle("~/bundles/scripts/account/register").Include(
               "~/Content/Libs/JQuery/Plugins/jquery.maskedinput.min.js",
               "~/Content/Scripts/Account/register.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/works/creatework").Include(
                "~/Content/Scripts/Works/creatework.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/works/editwork").Include(
                "~/Content/Scripts/Works/editwork.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/works/index").Include(
                "~/Content/Scripts/Works/worksindex.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/photos/editavatar").Include(
                "~/Content/Libs/Bootstrap/js/bootstrap-fileinput.js",
                "~/Content/Scripts/Photos/editavatar.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/photos/addphoto").Include(
                "~/Content/Libs/Bootstrap/js/bootstrap-fileinput.js",
                "~/Content/Scripts/Photos/addphoto.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/photos/index").Include(
                "~/Content/Scripts/Photos/photosindex.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/orders/index").Include(
                "~/Content/Libs/Mustache/mustache.min.js",
                "~/Content/Scripts/Orders/ordersIndex.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/orders/assignappointment").Include(
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.min.js",
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.ru.js",
                "~/Content/Scripts/Orders/appointmeeting.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/orders/upcomingorders").Include(
                "~/Content/Scripts/Orders/upcomingOrders.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/orders/edit").Include(
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.min.js",
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.ru.js",
                "~/Content/Libs/JQuery/Plugins/jquery.maskedinput.min.js",
                "~/Content/Scripts/Orders/edit.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/orders/create").Include(
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.min.js",
                "~/Content/Libs/Bootstrap/js/bootstrap-datetimepicker.ru.js",
                "~/Content/Libs/JQuery/Plugins/jquery.maskedinput.min.js",
                "~/Content/Scripts/Orders/create.js"));
        }
    }
}