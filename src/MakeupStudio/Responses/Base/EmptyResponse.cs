﻿namespace MakeupStudio.Responses
{
    /// <summary>
    /// Пустой ответ, содержащий только код ошибки и сообщение об ошибке
    /// </summary>
    public class EmptyResponse : BaseResponse
    {
    }
}