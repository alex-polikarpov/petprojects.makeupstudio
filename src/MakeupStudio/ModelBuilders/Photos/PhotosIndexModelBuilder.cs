﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Entities.Extensions;
using MakeupStudio.Core.Services;
using MakeupStudio.Models.Photos;

namespace MakeupStudio.ModelBuilders
{
    /// <summary>
    /// Билдер для модели отображения страницы фотографий
    /// </summary>
    public class PhotosIndexModelBuilder
    {
        private const int _PHOTO_WIDTH = 210;
        private readonly IPhotoService _photoService;
        private readonly IPhotosUtility _photosUtility;
        private readonly IFileSystemUtility _fileSystemUtility;

        public PhotosIndexModelBuilder(
            IPhotoService photoService,
            IPhotosUtility photosUtility,
            IFileSystemUtility fileSystemUtility)
        {
            _photoService = photoService;
            _photosUtility = photosUtility;
            _fileSystemUtility = fileSystemUtility;
        }

        /// <summary>
        /// Строит модель
        /// </summary>
        /// <param name="currentUser">Текущий пользователь</param>
        /// <param name="baseUrl">Базовый урл сайта</param>
        public async Task<PhotosIndexModel> Build(Account currentUser, string baseUrl)
        {
            var result = new PhotosIndexModel();
            var companyPhotos = await _photoService.GetCompanyPhotos(currentUser.CompanyId);            
            var avatar = companyPhotos.FilterByPhotoType(PhotoType.Avatar).FirstOrDefault();
            string avatarUrl = GetNoImageUrl();

            if (avatar != null)
            {
                avatarUrl = _photosUtility.GetFileAbsoluteUrl(avatar.RelativePath);
                avatarUrl = _photosUtility.ResizeImage(avatarUrl, _PHOTO_WIDTH);                
            }
            
            var photos = companyPhotos.FilterByPhotoType(PhotoType.Album).ToArray();
            var photoModels = new List<PhotoModel>();
            foreach (var photo in photos)
            {
                var absoluteUrl = _photosUtility.GetFileAbsoluteUrl(photo.RelativePath);

                photoModels.Add(new PhotoModel()
                {
                    Id = photo.Id,
                    Url = _photosUtility.ResizeImage(absoluteUrl, _PHOTO_WIDTH)
                });
            }

            result.AvatarUrl = avatarUrl;
            result.Photos = photoModels.ToArray();
            return result;
        }

        private string GetNoImageUrl()
        {
            var url = _photosUtility.GetFileAbsoluteUrl(_photosUtility.GetNoImageRelativePath());
            return _photosUtility.ResizeImage(url, _PHOTO_WIDTH);
        }
    }
}