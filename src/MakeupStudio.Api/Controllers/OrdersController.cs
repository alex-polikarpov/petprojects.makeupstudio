﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MakeupStudio.Api.Attributes;
using MakeupStudio.Api.Extensions;
using MakeupStudio.Api.Requests.Orders;
using MakeupStudio.Api.Responses;
using MakeupStudio.Business.Commands;

namespace MakeupStudio.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с заказами
    /// </summary>
    [RoutePrefix("Orders")]
    public class OrdersController : BaseApiController
    {
        /// <summary>
        /// Метод записи к мастеру
        /// </summary>
        [HttpPost]
        [Route("CreateBid")]
        [ValidateRequest]
        public async Task<JsonResult<EmptyResponse>> CreateBid([FromBody]CreateBidRequest request)
        {
            if (request == null)
                return Json(new EmptyResponse() {ErrorCode = 100, ErrorMessage = "Пустой запрос"});

            var commandArgs = new CreateBidCommandArgs()
            {
                CompanyId = request.CompanyId,
                ClientPhone = request.ClientPhone,
                ClientComment = request.ClientComment,
                ClientName = request.ClientName
            };

            var command = Resolve<ICommand<CreateBidCommandArgs, EmptyCommandResult>>();
            var commandResult = await command.Execute(commandArgs);
            var result = commandResult.ConverToResponse<EmptyResponse>();
            return Json(result);
        }
    }
}