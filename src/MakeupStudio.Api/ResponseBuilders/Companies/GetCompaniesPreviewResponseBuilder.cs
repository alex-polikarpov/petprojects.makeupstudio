﻿using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Api.Model;
using MakeupStudio.Api.Requests.Companies;
using MakeupStudio.Api.Responses.Companies;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Entities.Extensions;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Api.ResponseBuilders.Companies
{
    /// <summary>
    /// Билдер для ответа на запрос GetCompaniesPreviewResponse (получение превью информации о фирмах в зависимости от фильтров)
    /// </summary>
    public class GetCompaniesPreviewResponseBuilder
    {
        //Количество фотографий, которые надо отдавать для превью
        private const int _ALBUM_PHOTOS_COUNT = 3;
        //Ширина фоток в пикселях
        private const int _PHOTOS_WIDTH = 100;
        private const int _PHOTOS_HEIGHT = 100;
        private readonly ICompanyService _companyService;
        private readonly IWorkService _workService;
        private readonly IPhotoService _photoService;
        private readonly IPhotosUtility _photosUtility;

        public GetCompaniesPreviewResponseBuilder(
            ICompanyService companyService,
            IWorkService workService,
            IPhotoService photoService,
            IPhotosUtility photosUtility)
        {
            _companyService = companyService;
            _workService = workService;
            _photoService = photoService;
            _photosUtility = photosUtility;
        }

        public async Task<GetCompaniesPreviewResponse> Build(GetCompaniesPreviewRequest request)
        {
            //Получаем информацию о фирмах
            //
            var companiesInfo =
                await _companyService.SearchCompanies(request?.CategoriesId, request?.MinPrice, request?.MaxPrice);

            if (companiesInfo == null || companiesInfo.Length < 1)
                return new GetCompaniesPreviewResponse();

            //Получаем минимальные цены у фирм
            //
            var companiesId = companiesInfo.Select(x => x.Id).ToArray();
            var minPrices = await _workService.GetCompaniesMinPrices(companiesId);

            //Получаем фотографии фирмы
            //
            var photos = await _photoService.GetCompaniesPhotos(companiesId);

            //Формируем ответ
            //
            var previews = new List<CompanyPreviewModel>();
            foreach (var companyId in companiesId)
            {                
                var companyInfo = companiesInfo.Single(x => x.Id == companyId);
                var minPrice = minPrices.Single(x => x.CompanyId == companyId);
                var companyPhotos = photos.Where(x => x.CompanyId == companyId).ToList();

                var preview = new CompanyPreviewModel()
                {
                    Id = companyId,
                    Name = companyInfo.Name,
                    MinPrice = minPrice.MinPrice.Value                    
                };

                if (companyPhotos.Any())
                {                    
                    var noImageUrl = _photosUtility.GetFileAbsoluteUrl(_photosUtility.GetNoImageRelativePath());
                    noImageUrl = _photosUtility.ResizeImage(noImageUrl, _PHOTOS_WIDTH, _PHOTOS_HEIGHT);
                    var avatar = companyPhotos.FilterByPhotoType(PhotoType.Avatar).FirstOrDefault();
                    var albumPhotos = companyPhotos.FilterByPhotoType(PhotoType.Album).OrderBy(x=>x.Id).Take(_ALBUM_PHOTOS_COUNT).ToArray();
                    preview.PhotosCount = albumPhotos.Length;

                    var avatarUrl = noImageUrl;
                    if (avatar != null && !string.IsNullOrWhiteSpace(avatar.RelativePath))
                        avatarUrl = _photosUtility.GetFileAbsoluteUrl(avatar.RelativePath);    
                                       
                    preview.Avatar = _photosUtility.ResizeImage(avatarUrl, _PHOTOS_WIDTH, _PHOTOS_HEIGHT);
                    preview.Photos = new string[_ALBUM_PHOTOS_COUNT];

                    for (int i = 0; i < _ALBUM_PHOTOS_COUNT; i++)
                    {
                        if (albumPhotos.Length > i)
                        {
                            var photoRelativePath = albumPhotos[i].RelativePath;

                            if (!string.IsNullOrWhiteSpace(photoRelativePath))
                            {
                                var url = _photosUtility.GetFileAbsoluteUrl(photoRelativePath);
                                preview.Photos[i] = _photosUtility.ResizeImage(url, _PHOTOS_WIDTH, _PHOTOS_HEIGHT);
                            }
                            else preview.Photos[i] = noImageUrl;
                        }
                        else preview.Photos[i] = noImageUrl;
                    }             
                }

                previews.Add(preview);
            }

            return new GetCompaniesPreviewResponse()
            {
                Companies = previews.ToArray()
            };
        }
    }
}