﻿using MakeupStudio.Api.Model;

namespace MakeupStudio.Api.Responses.Companies
{
    /// <summary>
    /// Респонс на запрос получения информации о фирме
    /// </summary>
    public class GetCompanyInfoResponse : BaseResponse
    {
        /// <summary>
        /// Информация о фирме
        /// </summary>
        public CompanyInfoModel CompanyInfo { get; set; }

        /// <summary>
        /// Урл на аватарку
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Ссылки на фотографии
        /// </summary>
        public string[] Photos { get; set; }

        /// <summary>
        /// Информация о категориях и их услугах, которые оказывает фирма
        /// </summary>
        public CategoryModel[] Categories { get; set; }
    }
}