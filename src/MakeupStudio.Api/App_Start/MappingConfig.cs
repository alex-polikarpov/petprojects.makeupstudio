﻿using AutoMapper;
using MakeupStudio.Api.Model;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Utils.Mapping.Extensions;

namespace MakeupStudio.Api
{
    public class MappingConfig
    {
        public static MapperConfiguration Configure()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Company, CompanyInfoModel>()
                    .IgnoreSourceMember(x => x.Orders)
                    .IgnoreSourceMember(x => x.Photos)
                    .IgnoreSourceMember(x => x.Accounts)
                    .IgnoreSourceMember(x => x.Works);
            });

            return configuration;
        }
    }
}