﻿using System;
using System.Linq;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable
{
    /// <summary>
    /// Расширения для работы с заказами
    /// </summary>
    public static class OrderExtensions
    {
        /// <summary>
        /// Фильтрует заказы по айди фирмы
        /// </summary>
        public static IQueryable<Order> FilterByCompanyId(this IQueryable<Order> query, Guid companyId)
        {
            if (query == null)
                return null;

            return query.Where(x => x.CompanyId == companyId);
        }

        /// <summary>
        /// Фильтрует заказы по состоянию
        /// </summary>
        public static IQueryable<Order> FilterByState(this IQueryable<Order> query, OrderState state)
        {
            if (query == null)
                return null;

            return query.Where(x => x.OrderState == state);
        }
    }
}
