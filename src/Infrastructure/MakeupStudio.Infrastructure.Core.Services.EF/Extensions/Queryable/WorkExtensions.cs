﻿using System;
using System.Linq;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable
{
    public static class WorkExtensions
    {
        /// <summary>
        /// Фильтрует услуги по айдишникам категорий
        /// </summary>
        public static IQueryable<Work> FilterByCategories(this IQueryable<Work> query, int[] categoriesId)
        {
            if (query == null)
                return null;

            if (categoriesId == null || categoriesId.Length < 1)
                return query;

            categoriesId = categoriesId.Distinct().ToArray();

            return query.Where(x => categoriesId.Contains(x.CategoryId));
        }

        /// <summary>
        /// Фильтрует услуги по минимальной стоимости
        /// </summary>
        public static IQueryable<Work> FilterByMinPrice(this IQueryable<Work> query, decimal? minPrice)
        {
            if (query == null || !minPrice.HasValue)
                return query;

            if (minPrice.Value < Work.MIN_PRICE)
                minPrice = Work.MIN_PRICE;

            return query.Where(x => x.Price >= minPrice.Value);
        }

        /// <summary>
        /// Фильтрует услуги по максимальной стоимости
        /// </summary>
        public static IQueryable<Work> FilterByMaxPrice(this IQueryable<Work> query, decimal? maxPrice)
        {
            if (query == null || !maxPrice.HasValue)
                return query;

            if (maxPrice.Value > Work.MAX_PRICE)
                maxPrice = Work.MAX_PRICE;

            return query.Where(x => x.Price <= maxPrice.Value);
        }

        /// <summary>
        /// Фильтрует услуги по названию регистронезависимо
        /// </summary>
        public static IQueryable<Work> FilterByName(this IQueryable<Work> query, string name)
        {
            if (string.IsNullOrWhiteSpace(name) || query == null)
                return query;

            name = name.ToLower();
            return query.Where(x => x.Name.ToLower() == name);
        }

        public static IQueryable<Work> FilterByCompanyId(this IQueryable<Work> query, Guid companyId)
        {
            if (query == null)
                return null;

            return query.Where(x => x.CompanyId == companyId);
        }

        public static IQueryable<Work> FilterByCategoryId(this IQueryable<Work> query, int categoryId)
        {
            if (query == null)
                return null;

            return query.Where(x => x.CategoryId == categoryId);
        }
    }
}
