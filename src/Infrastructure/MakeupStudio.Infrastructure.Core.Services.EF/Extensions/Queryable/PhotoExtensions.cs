﻿using System;
using System.Linq;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable
{
    internal static class PhotoExtensions
    {
        public static IQueryable<Photo> FilterByCompanyId(this IQueryable<Photo> photos, Guid companyId)
        {
            if (companyId == default(Guid) || photos == null)
                return photos;

            return photos.Where(x => x.CompanyId == companyId);
        }

        public static IQueryable<Photo> FilterByPhotoType(this IQueryable<Photo> photos, PhotoType photoType)
        {
            if (photos == null)
                return null;

            return photos.Where(x => x.PhotoType == photoType);
        }
    }
}
