﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Данные категории "Косметология"
    /// </summary>
    internal static class CosmetologyCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Косметология",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Чистка лица").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Пилинг").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Мезотерапия").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Биоревитализация").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Неинвазивная карбокситерапия").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Увеличение губ").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Коррекция носогубных складок").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Другие процедуры").SetCategoryType(CategoryType.Works),
                }
            });
        }
    }
}
