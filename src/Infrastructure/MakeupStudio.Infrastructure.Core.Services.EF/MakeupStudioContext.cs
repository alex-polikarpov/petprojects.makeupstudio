﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class MakeupStudioContext : DbContext
    {
        #region DbSet-ы

        /// <summary>
        /// Фирмы
        /// </summary>
        public DbSet<Company> Companies { get; set; }

        /// <summary>
        /// Аккаунты
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Категории
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Заказы
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Фотографии
        /// </summary>
        public DbSet<Photo> Photos { get; set; }

        /// <summary>
        /// Услуги
        /// </summary>
        public DbSet<Work> Works { get; set; }

        /// <summary>
        /// Системные услуги
        /// </summary>
        public DbSet<SystemWorkName> SystemWorks { get; set; }

        #endregion



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ConfigureEntities(modelBuilder);
            ConfigureIndices(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }



        #region ConfigureEntities

        /// <summary>
        /// Настраивает сущности
        /// </summary>
        private void ConfigureEntities(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.Accounts)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Category>()
                .HasOptional(x => x.ParentCategory)
                .WithMany(x => x.ChildCategories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Work>()
                .HasRequired(x => x.Category)
                .WithMany(x => x.Works)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemWorkName>()
                .HasRequired(x => x.Category)
                .WithMany(x => x.SystemWorkNames)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Work>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.Works)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Photo>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.Photos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.Orders)
                .WillCascadeOnDelete(false);
        }

        #endregion



        #region ConfigureIndices

        /// <summary>
        /// Настраивает индексы
        /// </summary>
        private void ConfigureIndices(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(x => x.Email)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Account_Email_Unique", 0));

            modelBuilder.Entity<Account>()
                .Property(x => x.Phone)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Account_Phone_Unique", 0));

            modelBuilder.Entity<Photo>()
                .Property(x => x.RelativePath)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Relative_Path_Unique", 0));

            modelBuilder.Entity<Work>()
                .Property(x => x.CompanyId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Work_Name_Unique", 0));

            modelBuilder.Entity<Work>()
                .Property(x => x.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Work_Name_Unique", 1));

            modelBuilder.Entity<Work>()
                .Property(x => x.CategoryId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Work_Name_Unique", 2));

            modelBuilder.Entity<SystemWorkName>()
                .Property(x => x.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Work_Name_Unique", 0));

            modelBuilder.Entity<SystemWorkName>()
                .Property(x => x.CategoryId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Work_Name_Unique", 1));

            modelBuilder.Entity<Category>()
                .Property(x => x.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Category_Unique", 0));

            modelBuilder.Entity<Category>()
                .Property(x => x.CategoryType)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Category_Unique", 1));

            modelBuilder.Entity<Category>()
                .Property(x => x.ParentCategoryId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("Category_Unique", 2));
        }

        #endregion



        #region Вспомогательные методы

        private IndexAnnotation CreateUniqueIndex(string name, int order)
        {
            return new IndexAnnotation(new IndexAttribute(name) { IsUnique = true, Order = order });
        }

        #endregion
    }
}
