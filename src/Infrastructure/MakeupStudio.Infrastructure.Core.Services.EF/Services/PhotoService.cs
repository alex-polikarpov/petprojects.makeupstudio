﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class PhotoService : BaseService, IPhotoService
    {
        public PhotoService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает аватарку фирмы
        /// </summary>
        public async Task<Photo> GetAvatar(Guid companyId)
        {
            return await UnitOfWork.GetRepository<Photo>().Query()
                .FilterByCompanyId(companyId)
                .FilterByPhotoType(PhotoType.Avatar)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает фотографии фирмы
        /// </summary>
        /// <param name="companyId">Id компании</param>
        public async Task<Photo[]> GetCompanyPhotos(Guid companyId)
        {
            return await UnitOfWork.GetRepository<Photo>().Query()
                .AsNoTracking()
                .FilterByCompanyId(companyId)
                .ToArrayAsync();
        }

        /// <summary>
        /// Возвращает количество фотографий, которые есть у фирмы
        /// </summary>
        /// <param name="companyId"></param>
        public async Task<int> GetPhotosCount(Guid companyId)
        {
            return await UnitOfWork.GetRepository<Photo>().Query()
                .AsNoTracking()
                .FilterByCompanyId(companyId)
                .FilterByPhotoType(PhotoType.Album)
                .CountAsync();
        }

        /// <summary>
        /// Возвращает фотографию фирмы
        /// </summary>
        public async Task<Photo> GetPhoto(Guid companyId, int photoId)
        {
            return await UnitOfWork.GetRepository<Photo>().Query()
                .FilterByCompanyId(companyId)
                .FilterByPhotoType(PhotoType.Album)
                .Where(x => x.Id == photoId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает фотографии фирм
        /// </summary>
        /// <param name="companiesId">Id фирм</param>
        public async Task<Photo[]> GetCompaniesPhotos(Guid[] companiesId)
        {
            return await UnitOfWork.GetRepository<Photo>()
                .Query().AsNoTracking()
                .Where(x => companiesId.Contains(x.CompanyId))
                .ToArrayAsync();
        }
    }
}
