namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Guid(nullable: false),
                        Email = c.String(nullable: false, maxLength: 50),
                        Phone = c.String(maxLength: 20),
                        Name = c.String(maxLength: 50),
                        PasswordHash = c.String(nullable: false, maxLength: 500),
                        AccountState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId)
                .Index(t => t.Email, unique: true, name: "Account_Email_Unique")
                .Index(t => t.Phone, unique: true, name: "Account_Phone_Unique");
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 500),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        City = c.String(nullable: false, maxLength: 50),
                        Street = c.String(nullable: false, maxLength: 100),
                        HouseNumber = c.String(nullable: false, maxLength: 20),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        CompanyState = c.Int(nullable: false),
                        CompanyType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CompanyId = c.Guid(nullable: false),
                        ClientPhone = c.String(nullable: false, maxLength: 20),
                        ClientName = c.String(nullable: false, maxLength: 100),
                        ClientComment = c.String(maxLength: 200),
                        MasterComment = c.String(maxLength: 500),
                        CreatedDate = c.DateTime(nullable: false),
                        AppointedTime = c.DateTime(),
                        OrderState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Guid(nullable: false),
                        Description = c.String(maxLength: 500),
                        RelativePath = c.String(nullable: false, maxLength: 1000),
                        PhotoType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId)
                .Index(t => t.RelativePath, unique: true, name: "Relative_Path_Unique");
            
            CreateTable(
                "dbo.Works",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Guid(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => new { t.CompanyId, t.Name, t.CategoryId }, unique: true, name: "Work_Name_Unique");
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        CategoryType = c.Int(nullable: false),
                        ParentCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.ParentCategoryId)
                .Index(t => new { t.Name, t.CategoryType, t.ParentCategoryId }, unique: true, name: "Category_Unique");
            
            CreateTable(
                "dbo.SystemWorkNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => new { t.Name, t.CategoryId }, unique: true, name: "Work_Name_Unique");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Works", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Works", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.SystemWorkNames", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Categories", "ParentCategoryId", "dbo.Categories");
            DropForeignKey("dbo.Photos", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Orders", "CompanyId", "dbo.Companies");
            DropIndex("dbo.SystemWorkNames", "Work_Name_Unique");
            DropIndex("dbo.Categories", "Category_Unique");
            DropIndex("dbo.Works", "Work_Name_Unique");
            DropIndex("dbo.Photos", "Relative_Path_Unique");
            DropIndex("dbo.Photos", new[] { "CompanyId" });
            DropIndex("dbo.Orders", new[] { "CompanyId" });
            DropIndex("dbo.Accounts", "Account_Phone_Unique");
            DropIndex("dbo.Accounts", "Account_Email_Unique");
            DropIndex("dbo.Accounts", new[] { "CompanyId" });
            DropTable("dbo.SystemWorkNames");
            DropTable("dbo.Categories");
            DropTable("dbo.Works");
            DropTable("dbo.Photos");
            DropTable("dbo.Orders");
            DropTable("dbo.Companies");
            DropTable("dbo.Accounts");
        }
    }
}
