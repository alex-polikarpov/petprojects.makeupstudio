﻿namespace MakeupStudio.Common.Utils.FileSystem
{
    /// <summary>
    /// Утилита для работы с файлам и папками
    /// </summary>
    public interface IFileSystemUtility
    {
        /// <summary>
        /// Возвращает абсолютный путь к Temp папке
        /// </summary>
        string TempDirectory { get; }

        /// <summary>
        /// Возвращает абсолютый путь к папке с файлами
        /// </summary>
        string FilesDirectory { get; }



        /// <summary>
        /// Проверяет создана ли директория
        /// </summary>
        /// <param name="absolutePath">Полный путь к папке</param>
        bool DirectoryIsExists(string absolutePath);

        /// <summary>
        /// Создает папку и подпапки если она(они) еще не создана
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к создаваемой папке</param>
        void CreateDirectory(string absolutePath);

        /// <summary>
        /// Проверяет создан ли файл
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        bool FileIsExists(string absolutePath);

        /// <summary>
        /// Удаляет файл
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        void DeleteFile(string absolutePath);

        /// <summary>
        /// Возвращает полный путь к файлу или null если такого файла нет. 
        /// </summary>
        /// <param name="relativePath">Относительный путь к файлу</param>
        string GetAbsoluteFilePath(string relativePath);

        /// <summary>
        /// Возвращает относительный путь к файлу
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        string GetRelativeFilePath(string absolutePath);

        /// <summary>
        /// Перемещает файл
        /// </summary>
        ///<param name="sourceAbsolutePath">Абсолютный путь к файлу</param>
        /// <param name="destinationAbsolutePath">Абсолютный путь файла куда нужно переместить файл.</param>
        void MoveFile(string sourceAbsolutePath, string destinationAbsolutePath);

        /// <summary>
        /// Комбинирует путь и возвращает новый путь
        /// </summary>
        /// <param name="path1">Первая часть пути</param>
        /// <param name="path2">Вторая часть пути</param>
        string CombinePath(string path1, string path2);

        /// <summary>
        /// Возвращает имя файла исходя из его абсолютного пути
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        string GetFileName(string absolutePath);

        /// <summary>
        /// Возвращает расширение файла с точкой
        /// </summary>
        /// <param name="absolutePath">абсолютный путь к файлу</param>
        string GetFileExtension(string absolutePath);
    }
}
