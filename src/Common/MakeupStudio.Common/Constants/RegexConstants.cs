﻿namespace MakeupStudio.Common.Constants
{
    /// <summary>
    /// Константы для регулярных выражений
    /// </summary>
    public static class RegexConstants
    {
        /// <summary>
        /// Пароль. Формат хотя бы одна цифра и хотя бы одна буква
        /// </summary>
        public const string PASSWORD = @"^\d+[a-zA-Z]+[a-zA-Z0-9]*|[a-zA-Z]+\d+[a-zA-Z0-9]*$";

        /// <summary>
        /// Номер телефона формат +7-999-999-99-99
        /// </summary>
        public const string PHONE = @"^\+7-\d{3}-\d{3}-\d{2}-\d{2}$";

        /// <summary>
        /// Целое число
        /// </summary>
        public const string NUMBER = @"^\d+$";

        /// <summary>
        /// Регулярка для имени
        /// </summary>
        public const string NAME = @"^[а-яА-Я]*$";

        /// <summary>
        /// Комментарий. 
        /// Допустимы русские, англ символы
        /// Цифры
        /// Пунктуация
        /// Любой знак пробела (пробел, табуляция и перевод страницы)
        /// </summary>
        public const string COMMENT = @"^[а-яА-Я0-9a-zA-Z:.,;\!\?\-_'""\s]*";
    }
}