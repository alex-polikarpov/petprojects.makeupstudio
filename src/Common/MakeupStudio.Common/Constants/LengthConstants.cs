﻿namespace MakeupStudio.Common.Constants
{
    /// <summary>
    /// Константы с мин. и макс. размером длины полей
    /// </summary>
    public static class LengthConstants
    {
        /// <summary>
        /// Минимальная длина Email-а
        /// </summary>
        public const int EMAIL_MIN = 5;

        public const int STREET_MIN = 2;

        public static class Password
        {
            public const int MIN = 8;
            public const int MAX = 30;
        }

        /// <summary>
        /// Максимальный вес одной фотографии
        /// </summary>
        public const int PHOTO_MAX_SIZE = 2100000;
    }
}