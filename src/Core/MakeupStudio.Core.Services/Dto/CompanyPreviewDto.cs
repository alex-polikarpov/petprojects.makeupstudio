﻿using System;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Превьюшка фирмы 
    /// </summary>
    public class CompanyPreviewDto
    {
        /// <summary>
        /// Id фирмы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название фирмы
        /// </summary>
        public string Name { get; set; }
    }
}
