﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Состояние фирмы
    /// </summary>
    public enum CompanyState
    {
        /// <summary>
        /// Не активна (не будет показываться пользователям)
        /// </summary>
        NotActive = 0, 

        /// <summary>
        /// Активна
        /// </summary>
        Active = 1
    }
}
