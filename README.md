# PetProjects.MakeupStudio

Pet project для различных услуг выполнял в ноябре 2016. Это была подработка шабашка, заказчик забросил его.
Суть проекта - сделать агрегатор для различных услуг по красоте и здоровью: макияж, массаж, ногтевой сервис и т.д.
По этой идее должны были регистрироваться различные мастера, размещать цены на свои услуги, фотографии и т.д., а их клиенты через мобильное приложение выбирать мастеров и записываться к ним. 

#### Документация
* Скриншоты админки
    * [Страница входа и регистрации](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/Screenshots/signin_screenshots.md)
    * [Раздел услуг](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/Screenshots/works-section_screenshots.md)
    * [Раздел фотографий](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/Screenshots/fotos-section_screenshots.md)
    * [Раздел заказов](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/Screenshots/bids-section_screenshots.md)
* Техническая документация
    * [Схема БД](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/database-scheme.md)
    * [Архитектура](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/architecture.md)
    * [Мобильное Api](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/mobile-api.md)

#### Использованные технологии и подходы
* .NET Framework 4.5.2, ASP.NET MVC 5, ASP.NET WebApi;
* SQL Sever, Entity Framework 6;
* Луковая (Onion) архитектура, Трансформации конфигов (Microsoft.Web.Publishing.Tasks);
* Autofac (IoC container), AutoMapper, Microsoft.Owin, Microsoft.AspNet.Cors;
* ImageResizer, Geocoding.net;
* Html, Css, Jquery, Bootstrap.

#### Запуск админки
* Поправить настройки подключения к БД в Web.apolikarpov.config, выбрать конфигурацию apolikarpov и сделать билд;
* Поправить при необходимости настройку BaseFilesUri;
* Выбрать запускаемый проект (csproj) MakeupStudio;
* При первом запуске создается БД и предзаполняется данными, а также создается одна тестовая фирма.

#### Пользователи для входа: 
* Логин abc@mail.ru пароль Qwerty123 - тестовый пользователь для входа в админку.